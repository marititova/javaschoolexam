package com.tsystems.javaschool.tasks.calculator;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        
        if (statement == null) {
            return null;
        }
        if (statement == "") {
            return null;
        }
        if (statement.substring(0,1).equals("-")) {
            return null;
        }
        
        int parOpen = 0;
        int parClose = 0;
        for (int i = 0; i < statement.length(); i++) {
            if (statement.substring(i,i+1).equals(",")) {
                return null;
            }
            if ((i != statement.length()-1) && (statement.substring(i,i+1).equals(".")) &&
                (statement.substring(i+1,i+2).equals("."))) {
                return null;
            }
            if ((i <= statement.length()-6) && (statement.substring(i,i+1).equals("/")) &&
                (statement.substring(i+1,i+2).equals("(")) && 
                (statement.substring(i+2,i+3).equals(statement.substring(i+4,i+5)) &&
                (statement.substring(i+3,i+4)).equals("-"))) {
                return null;
            }
            if ((i != statement.length()-1) && (statement.substring(i,i+1).equals("+")) &&
                (statement.substring(i+1,i+2).equals("+"))) {
                return null;
            }
            if ((i != statement.length()-1) && (statement.substring(i,i+1).equals("-")) &&
                (statement.substring(i+1,i+2).equals("-"))) {
                return null;
            }
            if ((i != statement.length()-1) && (statement.substring(i,i+1).equals("*")) &&
                (statement.substring(i+1,i+2).equals("*"))) {
                return null;
            }
            if ((i != statement.length()-1) && (statement.substring(i,i+1).equals("/")) &&
                (statement.substring(i+1,i+2).equals("/"))) {
                return null;
            }
            if (statement.substring(i,i+1).equals("(")) {
                parOpen++;
            }
            if (statement.substring(i,i+1).equals(")")) {
                parClose++;
            }
        }
        if (parOpen != parClose) {
            return null;
        }
        
        int parOpenPointer = 0;
        int parClosePointer = 0;
        String[] numbers;
        String[] ops;  
        for (int i = 0; i < statement.length(); i++) {
            if (statement.substring(i,i+1).equals("(")) {
            parOpenPointer = i;
            }
            if (statement.substring(i,i+1).equals(")")) {
            parClosePointer = i;
            }
        }
        if (parOpenPointer != 0) {
            String s = statement.substring(parOpenPointer+1,parClosePointer);
            numbers = s.split("[\\s()/*+-]+");
            ops = s.split("[0-9.]+");
            double[] m = calculate(numbers,ops);
            if (m[0] > 0) {
                statement = statement.substring(0,parOpenPointer) + m[0] + statement.substring(parClosePointer+1,statement.length()); 
                numbers = statement.split("[\\s()/*+-]+");
                ops = statement.split("[0-9.]+");
            } else {
                statement = statement.substring(0,parOpenPointer) + "#" + statement.substring(parClosePointer+1,statement.length()); 
                numbers = statement.split("[\\s()/*+-]+");
                for (int i = 0; i < numbers.length; i++) {
                    if (numbers[i].equals("#")) {
                        numbers[i] = "" + m[0];
                    }
                }
                ops = statement.split("[0-9.#]+");
            }
        } else {
            numbers = statement.split("[\\s()/*+-]+");
            ops = statement.split("[0-9.]+");  
        }

        double[] n = calculate(numbers,ops);

        String s = "" + n[0];
        int index = 0;
        for (int i = 0; i < s.length(); i++) {
            if (s.substring(i,i+1).equals(".")) {
                index = i;
                break;
            }
        }
        int count2 = 0;
        for (int i = index + 1; i < s.length(); i++) {
            if (s.substring(i,i+1).equals("0")) {
                count2++;
            }
        }
        String result = "";
        if (count2 == (s.length() - (index+1))) {
            result = s.substring(0,index);
        } else {
            result = s;
        }
        return result;
    }

    private static double[] calculate(String[] numbers,String[] ops) {

       double[] num = new double[numbers.length];
       double[] num2 = new double[numbers.length];
       String[] ops2 = new String[ops.length];
       ops[0] = "!";
       for (int i = 0; i < num.length; i++) {
           num[i] = Double.parseDouble(numbers[i]);
       }
       
       double div = 0;
       double mul = 0;
       double sub = 0;
       double add = 0;
       int pointerMul = 0;
       int pointerDiv = 0;
       int pointerSub = 0;
       int pointerAdd = 0;
       int j = 0;
       int count = ops.length-1;

       while(true) {

        pointerMul = 0;
        pointerDiv = 0;
        pointerSub = 0;
        pointerAdd = 0;
        
            for (int i = 0; i < ops.length; i++) {
                if (ops[i].equals("/")) {
                    pointerDiv = i;
                    
                }  
                if (ops[i].equals("*")) {
                    pointerMul = i;
                    
                } 
                if (ops[i].equals("-")) {
                    pointerSub = i;
                    
                }
                if (ops[i].equals("+")) {
                    pointerAdd = i;
                
                } 
            }
            if (pointerDiv != 0) {
                div = num[pointerDiv-1]/num[pointerDiv];
                j = 0;
                num2 = new double[num.length-1];
                ops2 = new String[ops.length-1];
                for (int i = 0; i < num2.length; i++) {
                    if (i != pointerDiv-1) {
                        num2[i] = num[i+j];
                        ops2[i] = ops[i+j];
                    } else {
                        num2[i] = div;
                        ops2[i] = ops[i];
                        j++;
                    }
                }
                pointerMul = 0;
                pointerSub = 0;
                pointerAdd = 0;
            }
                       
            if (pointerMul != 0) { 
                mul = num[pointerMul-1] * num[pointerMul];
                j = 0;
                num2 = new double[num.length-1];
                ops2 = new String[ops.length-1];
                for (int i = 0; i < num2.length; i++) {
                    if (i != pointerMul-1) {
                        num2[i] = num[i+j];
                        ops2[i] = ops[i+j];
                    } else {
                        num2[i] = mul;
                        ops2[i] = ops[i];
                        j++; 
                    }
                }
                pointerSub = 0;
                pointerAdd = 0;
            }
            
            if (pointerSub != 0) {
                sub = num[pointerSub-1]-num[pointerSub];
                j = 0;
                num2 = new double[num.length-1];
                ops2 = new String[ops.length-1];
                for (int i = 0; i < num2.length; i++) {
                    if (i != pointerSub-1) {
                        num2[i] = num[i+j];
                        ops2[i] = ops[i+j];
                    } else {
                        num2[i] = sub;
                        ops2[i] = ops[i]; 
                        j++;
                    }
                }
                pointerAdd = 0;
            }

            if (pointerAdd != 0) {
                add = num[pointerAdd-1]+num[pointerAdd];
                j = 0;
                num2 = new double[num.length-1];
                ops2 = new String[ops.length-1];
                for (int i = 0; i < num2.length; i++) { 
                    ops2[0] = "!";
                    if (i != pointerAdd-1) {
                        num2[i] = num[i+j];
                        ops2[i] = ops[i+j];
                    } else {
                        num2[i] = add;
                        ops2[i] = ops[i];
                        j++;
                    }
                }
            }
            
            num = num2;
            num2 = null;
            
            ops = ops2;
            ops2 = null;
            
            count--;
            if (count == 0) {
                break;
            } 
        }
        return num;
    }
}
