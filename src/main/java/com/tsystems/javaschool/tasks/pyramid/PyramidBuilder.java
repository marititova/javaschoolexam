package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;
import java.util.Collections;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) throws CannotBuildPyramidException {
                
        int level = 0;
        int x = inputNumbers.size();
        
        for (int n = 0; n < inputNumbers.size(); n++) {
            try {
                if (inputNumbers.get(n).equals(null)) {
                    return new int[0][0];
                }
            } catch (NullPointerException e) {
                throw new CannotBuildPyramidException(); 
            }
        }

        for (int i = 1; i < inputNumbers.size(); i++) {
            x = x - i;
            if (x == 0) {
                level = i;
                break;
            }
            if (x < 0) {
                throw new CannotBuildPyramidException();
            }
        }
        int num = level;
        int breadth = level + (level - 1);
        
        int[][] pyramid = new int[num][breadth];
        for(int i = 0; i < num; ++i) {
            for(int j = 0; j < breadth; ++j) {
                pyramid[i][j] = 0;
            }
        }
        Collections.sort(inputNumbers, Collections.reverseOrder());
        int count = 1;
        int count2 = 0;
        int m = 0;
        System.out.println(inputNumbers);
        
        for (int i = level - 1; i >= 0; i--) {
            m = i + 1;
            loop:
            for (int j = breadth - count; j > -1; j = j - 2) {
                if (m == 0) {
                    break loop;
                }
                pyramid[i][j] = inputNumbers.get(count2);
                m--;
                count2++;
            }
            count++;
        }
        return pyramid;
    }
}
